enum Status {
  criado, iniciado, cancelado, finalizado
}

class OrdemDeServico {
  int numero;
  Status situacao;
  String nomeCliente;
  
  String toString() {
    return '''
Nº: $numero
Situação: ${situacao.toString().split('.')[1]}
Cliente: $nomeCliente'''
  }
}

void main() {
  var cadastro = <OrdemDeServico>[];
  
  var p = OrdemDeServico();
  p.numero = 923493;
  p.situacao = Status.cancelada;
  p.nomeCliente = "Beltrano da Silva";
  
  cadastro.add(p);
  
  p = OrdemDeServico();
  p.numero = 83273;
  p.situacao = Status.em_execucao;
  p.nomeCliente = "Ciclano de Souza";
  
  cadastro.add(p);
  
  
  for(var x in cadastro)
    print('$x');
	}